package com.uphave.galaxy.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.uphave.galaxy.GalaxyCallback;
import com.uphave.galaxy.GalaxyRequestError;
import com.uphave.galaxy.GalaxyService;
import com.uphave.galaxy.LoginActivity;
import com.uphave.galaxy.R;
import com.uphave.galaxy.Utils;
import com.uphave.galaxy.model.Profile;

import butterknife.Bind;
import butterknife.ButterKnife;


public class ProfileFragment extends Fragment {
    private GalaxyService mService;
    private Profile mProfile;
    private ViewHolder mViewHolder;

    public ProfileFragment() {
    }

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService = new GalaxyService();
        mService.setAccessToken(Utils.loadAccessToken(getContext()));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, view);

        mViewHolder.retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                retry();
            }
        });
        mViewHolder.btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        mViewHolder.change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });
        mViewHolder.update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUserInfo();
            }
        });
        if (mProfile == null) {
            retry();
        } else {
            showProfile();
        }

        return view;
    }

    private void updateUserInfo() {
        String email = mViewHolder.email.getText().toString();
        if (!email.equals(mProfile.getEmail())) {
            mViewHolder.progress.setVisibility(View.VISIBLE);
            mService.update(mProfile.getUserId(), null, email, new GalaxyCallback<Profile>() {
                @Override
                public void onSuccess(Profile profile) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mViewHolder != null) {
                                mViewHolder.progress.setVisibility(View.GONE);
                            }
                            Toast.makeText(getContext(), R.string.profile_updated, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onError(GalaxyRequestError error) {
                    Utils.alert(getActivity(), getActivity().getString(R.string.alert_update_profile));
                    if (mViewHolder != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mViewHolder.progress.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });
        }
    }

    private void changePassword() {
        //noinspection ConstantConditions
        String pass = mViewHolder.edtNewPassword.getEditText().getText().toString();
        //noinspection ConstantConditions
        String pass2 = mViewHolder.edtNewPassword2.getEditText().getText().toString();
        if (pass.equals(pass2)) {
            mViewHolder.progress.setVisibility(View.VISIBLE);
            mService.update(mProfile.getUserId(), pass, mProfile.getEmail(), new GalaxyCallback<Profile>() {
                @Override
                public void onSuccess(Profile profile) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mViewHolder != null) {
                                mViewHolder.progress.setVisibility(View.GONE);
                            }
                            Toast.makeText(getContext(), R.string.profile_updated, Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onError(GalaxyRequestError error) {
                    Utils.alert(getActivity(), getActivity().getString(R.string.alert_update_profile));
                    if (mViewHolder != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mViewHolder.progress.setVisibility(View.GONE);
                            }
                        });
                    }
                }
            });
        } else {
            mViewHolder.edtNewPassword2.setError(getString(R.string.password_mismatch));
        }
    }

    @Override
    public void onDestroyView() {
        mViewHolder = null;
        super.onDestroyView();
    }

    private void logout() {
        new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog)
                .setTitle(R.string.confirm)
                .setMessage(R.string.confirm_sign_out)
                .setCancelable(true)
                .setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        Utils.removeAccessToken(getContext());
                        startActivity(new Intent(getContext(), LoginActivity.class));
                        getActivity().finish();
                    }
                })
                .show();
    }

//   private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
//      public DownloadImageTask() {
//      }
//
//      protected Bitmap doInBackground(String... urls) {
//         String url = urls[0];
//         Bitmap result = null;
//         try {
//            InputStream in = new java.net.URL(url).openStream();
//            result = BitmapFactory.decodeStream(in);
//         } catch (Exception e) {
//            Log.e("Error", e.getMessage());
//            e.printStackTrace();
//         }
//         return result;
//      }
//
//      protected void onPostExecute(Bitmap result) {
//         if (result != null) {
//            mAvatar = result;
//         }
//         if (mViewHolder != null) {
//            mViewHolder.avatar.setImageBitmap(result);
//         }
//      }
//   }


    private void retry() {
        mViewHolder.retry.setVisibility(View.GONE);
        mViewHolder.main.setVisibility(View.GONE);
        mViewHolder.progress.setVisibility(View.VISIBLE);

        mService.getProfile(new GalaxyCallback<Profile>() {
            @Override
            public void onSuccess(final Profile profile) {
                mProfile = profile;
                if (mViewHolder != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showProfile();
                        }
                    });
                }
            }

            @Override
            public void onError(final GalaxyRequestError error) {
                Utils.alert(getActivity(), getActivity().getString(R.string.alert_profile));
                if (mViewHolder != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mViewHolder.progress.setVisibility(View.GONE);
                            mViewHolder.main.setVisibility(View.GONE);
                            mViewHolder.retry.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }
        });
    }


    private void showProfile() {
        mViewHolder.retry.setVisibility(View.GONE);
        mViewHolder.progress.setVisibility(View.GONE);
        mViewHolder.main.setVisibility(View.VISIBLE);

        mViewHolder.username.setText(String.format("%s", mProfile.getUserId()));
        mViewHolder.email.setText(String.format("%s", mProfile.getEmail()));
    }

    static final class ViewHolder {
        @Bind(R.id.progress)
        View progress;
        @Bind(R.id.main)
        View main;
        @Bind(R.id.retry)
        View retry;

        @Bind(R.id.username)
        TextView username;
        @Bind(R.id.logout)
        ImageButton btnLogout;
        @Bind(R.id.new_password)
        TextInputLayout edtNewPassword;
        @Bind(R.id.new_password_2)
        TextInputLayout edtNewPassword2;
        @Bind(R.id.change)
        Button change;
        @Bind(R.id.update)
        Button update;
        @Bind(R.id.edt_email)
        EditText email;
    }
}
