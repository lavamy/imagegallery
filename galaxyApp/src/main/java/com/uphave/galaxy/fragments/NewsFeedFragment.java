package com.uphave.galaxy.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uphave.galaxy.GalaxyCallback;
import com.uphave.galaxy.GalaxyRequestError;
import com.uphave.galaxy.GalaxyService;
import com.uphave.galaxy.R;
import com.uphave.galaxy.Utils;
import com.uphave.galaxy.model.NewsFeed;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;


public class NewsFeedFragment extends Fragment {
    private static final String TAG = NewsFeedFragment.class.getSimpleName();
    private static final int PICK_IMAGE = 123;
    private GalaxyService mService;
    private List<NewsFeed> mNewsFeeds;
    private ViewHolder mViewHolder;
    private NewsFeedAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private Map<String, Bitmap> mCaches = new HashMap<>();

    public NewsFeedFragment() {
    }

    public static NewsFeedFragment newInstance() {
        return new NewsFeedFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService = new GalaxyService();
        mService.setAccessToken(Utils.loadAccessToken(getContext()));
        mNewsFeeds = new ArrayList<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news_feed, container, false);

        mViewHolder = new ViewHolder();
        ButterKnife.bind(mViewHolder, view);

        View.OnClickListener upload = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        };

        mViewHolder.upload.setOnClickListener(upload);

        mLayoutManager = new LinearLayoutManager(getContext());
        mViewHolder.recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NewsFeedAdapter(getActivity(), mNewsFeeds);
        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                if (mViewHolder != null) {
                    mViewHolder.empty.setVisibility(mNewsFeeds.size() > 0 ? View.GONE : View.VISIBLE);
                    mViewHolder.recyclerView.setVisibility(mNewsFeeds.size() > 0 ? View.VISIBLE : View.GONE);
                }
            }

        });

        mViewHolder.recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();

        mViewHolder.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();

            }
        });
        mViewHolder.swipeRefreshLayout.setEnabled(false);
        mViewHolder.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState != RecyclerView.SCROLL_STATE_IDLE) return;
                try {
                    int firstPos = mLayoutManager.findFirstCompletelyVisibleItemPosition();
                    if (firstPos > 0) {
                        mViewHolder.swipeRefreshLayout.setEnabled(false);
                    } else {
                        mViewHolder.swipeRefreshLayout.setEnabled(true);
//                        if (mViewHolder.recyclerView.getScrollState() == 1)
//                            if (mViewHolder.swipeRefreshLayout.isRefreshing())
//                                mViewHolder.recyclerView.stopScroll();
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Scroll Error : " + e.getLocalizedMessage());
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

        });
        refresh();

        return view;
    }

    private void refresh() {
        mService.browser(new Date(), new Date(), new GalaxyCallback<List<NewsFeed>>() {
            @Override
            public void onSuccess(final List<NewsFeed> newsFeeds) {
                Log.d("DB", "" + newsFeeds);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mViewHolder != null) {
                            mViewHolder.swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    mViewHolder.swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }
                        mNewsFeeds.clear();
                        mNewsFeeds.addAll(newsFeeds);
                        Collections.reverse(mNewsFeeds);
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onError(GalaxyRequestError error) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (mViewHolder != null) {
                            mViewHolder.swipeRefreshLayout.post(new Runnable() {
                                @Override
                                public void run() {
                                    mViewHolder.swipeRefreshLayout.setRefreshing(false);
                                }
                            });
                        }
                        Toast.makeText(getContext(), "Cannot refresh news feeds", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    @Override
    public void onDestroyView() {
        mViewHolder = null;
        super.onDestroyView();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE) {
            final Uri image = data.getData();
            View radioGroupView = View.inflate(getContext(), R.layout.layout_status, null);
            final EditText status = (EditText) radioGroupView.findViewById(R.id.status);

            new AlertDialog.Builder(getContext(), R.style.AppTheme_Dialog)
                    .setTitle("Update Status")
                    .setView(radioGroupView)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            doUpload(image, status.getText().toString());
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // Canceled.
                        }
                    })
                    .show();

        }
    }

    private void doUpload(Uri image, String s) {
        if (image == null || TextUtils.isEmpty(s)) {
            Utils.alert(getActivity(), "Invalid data");
            return;
        }
        mViewHolder.progress.setVisibility(View.VISIBLE);
        try {
            InputStream inputStream = getContext().getContentResolver().openInputStream(image);
            byte[] bytes = readBytes(inputStream);
            mService.upload(bytes, s, new GalaxyCallback<NewsFeed>() {
                @Override
                public void onSuccess(NewsFeed newsFeed) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mViewHolder.progress.setVisibility(View.GONE);
                            refresh();
                            Toast.makeText(getContext(), "Upload successful", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onError(GalaxyRequestError error) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mViewHolder.progress.setVisibility(View.GONE);
                            Utils.alert(getActivity(), "Cannot upload your picture. Please try again!");
                        }
                    });
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Cannot upload picture", Toast.LENGTH_SHORT).show();
        }
    }

    public byte[] readBytes(InputStream inputStream) throws IOException {
        // this dynamically extends to take the bytes you read
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

        // this is storage overwritten on each iteration with bytes
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        // we need to know how may bytes were read to write them to the byteBuffer
        int len;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }

        // and then we can return your byte array.
        return byteBuffer.toByteArray();
    }

    static final class ViewHolder {
        @Bind(R.id.progress)
        View progress;

        @Bind(R.id.upload)
        View upload;
        @Bind(R.id.my_recycler_view)
        RecyclerView recyclerView;
        @Bind(R.id.empty)
        View empty;
        @Bind(R.id.main)
        SwipeRefreshLayout swipeRefreshLayout;
    }

    public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {
        private List<NewsFeed> mData;
        private Context mActivity;

        public NewsFeedAdapter(Context activity, List<NewsFeed> data) {
            mData = data;
            mActivity = activity;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news_feed, parent, false);
            ViewHolder vh = new ViewHolder(v);

            return vh;
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final NewsFeed summarization = mData.get(position);
            holder.status.setText(summarization.getStatus());
            if (summarization.getDate() == null) {
                holder.time.setText("Unknown");
            } else {
                holder.time.setText(java.text.SimpleDateFormat.getDateTimeInstance().format(summarization.getDate().getTime()));
            }
            if (!TextUtils.isEmpty(summarization.getImage())) {
                if (mCaches.containsKey(summarization.getImage())) {
                    holder.image.setImageBitmap(mCaches.get(summarization.getImage()));
                } else {
                    new AsyncTask<String, Void, Bitmap>() {
                        @Override
                        protected Bitmap doInBackground(String... params) {
                            Bitmap bm = null;
                            try {
                                URL aURL = new URL(params[0]);
                                URLConnection conn = aURL.openConnection();
                                conn.connect();
                                InputStream is = conn.getInputStream();
                                BufferedInputStream bis = new BufferedInputStream(is);
                                bm = BitmapFactory.decodeStream(bis);
                                bis.close();
                                is.close();
                            } catch (IOException e) {
                                Log.e("Hub", "Error getting the image from server : " + e.getMessage());
                            }
                            return getResizedBitmap(bm, 1000);
                        }

                        @Override
                        protected void onPostExecute(Bitmap bitmap) {
                            super.onPostExecute(bitmap);
                            mCaches.put(summarization.getImage(), bitmap);
                            holder.image.setImageBitmap(bitmap);
                        }

                        public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
                            int width = image.getWidth();
                            int height = image.getHeight();

                            float bitmapRatio = (float) width / (float) height;
                            if (bitmapRatio > 0) {
                                width = maxSize;
                                height = (int) (width / bitmapRatio);
                            } else {
                                height = maxSize;
                                width = (int) (height * bitmapRatio);
                            }
                            return Bitmap.createScaledBitmap(image, width, height, true);
                        }

                    }.execute(summarization.getImage());

                }
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            @Bind(R.id.status)
            TextView status;
            @Bind(R.id.time)
            TextView time;
            @Bind(R.id.image)
            ImageView image;
            @Bind(R.id.card_view)
            CardView cardView;

            public ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
}
