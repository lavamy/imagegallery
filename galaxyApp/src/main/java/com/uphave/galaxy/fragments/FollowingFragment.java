package com.uphave.galaxy.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.uphave.galaxy.GalaxyService;
import com.uphave.galaxy.R;
import com.uphave.galaxy.Utils;
import com.uphave.galaxy.model.Profile;

import java.util.List;

import butterknife.Bind;

public class FollowingFragment extends Fragment {
    private static final String TAG = FollowingFragment.class.getSimpleName();
    @Bind(R.id.progress)
    ProgressBar mProgressBar;
//   @Bind(R.id.swipe_layout)
//   SwipeRefreshLayout mSwipeRefreshLayout;

    private LinearLayoutManager mLayoutManager;
    private View view;
    private GalaxyService mService;
    private List<Profile> mUsers;

    public FollowingFragment() {
    }

    public static FollowingFragment newInstance() {
        return new FollowingFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mService = new GalaxyService();
        mService.setAccessToken(Utils.loadAccessToken(getContext()));
        mLayoutManager = new LinearLayoutManager(getContext());
//      mAdapter = new MyAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_following, container, false);
        //ButterKnife.bind(view);

//      mRecyclerView.setLayoutManager(mLayoutManager);
//      mRecyclerView.setAdapter(mAdapter);

//      if (mUsers == null) {
//         getFollowingUsers();
//      } else {
//         showFollowingUsers();
//      }

        return view;
    }

    @Override
    public void onDestroyView() {
        //ButterKnife.unbind(view);
        view = null;
        super.onDestroyView();
    }

//   private void getFollowingUsers() {
//      mViewHolder.retry.setVisibility(View.GONE);
//      mViewHolder.main.setVisibility(View.GONE);
//      mViewHolder.progress.setVisibility(View.VISIBLE);
//
//      mService.getFollowingUsers(new GalaxyCallback<List<Profile>>() {
//         @Override
//         public void onSuccess(final List<Profile> profiles) {
//            mUsers = profiles;
//            if (view != null) {
//               getActivity().runOnUiThread(new Runnable() {
//                  @Override
//                  public void run() {
//                     showFollowingUsers();
//                  }
//               });
//            }
//         }
//
//         @Override
//         public void onError(final GalaxyRequestError error) {
//            Utils.alert(getActivity(), getActivity().getString(R.string.alert_following));
//            if (view != null) {
//               getActivity().runOnUiThread(new Runnable() {
//                  @Override
//                  public void run() {
//                     mProgressBar.setVisibility(View.GONE);
//                     mViewHolder.main.setVisibility(View.GONE);
//                     mViewHolder.retry.setVisibility(View.VISIBLE);
//                  }
//               });
//            }
//         }
//      });
//   }
//
//   private void showFollowingUsers() {
//      mViewHolder.retry.setVisibility(View.GONE);
//      mViewHolder.progress.setVisibility(View.GONE);
//      mViewHolder.main.setVisibility(View.VISIBLE);
//
//      mViewHolder.id.setText(String.format("%s", mUsers.getId()));
//      mViewHolder.deviceType.setText(String.format("%s", mUsers.getDeviceType()));
//      mViewHolder.serialNumber.setText(String.format("%s", mUsers.getSerialNumber()));
//      mViewHolder.firmwareVersion.setText(String.format("%s", mUsers.getFirmwareVersion()));
//      mViewHolder.batteryLevel.setText(String.format("%d", mUsers.getBatteryLevel()));
//      mViewHolder.lastSync.setText(String.format("%s", mUsers.getLastSyncTime()));
//   }
}
