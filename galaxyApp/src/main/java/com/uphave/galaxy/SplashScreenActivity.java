package com.uphave.galaxy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.uphave.galaxy.model.Profile;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends AppCompatActivity {
    private static final int MIN_DELAY = 3000;
    private final Object mLock = new Object();
    private boolean mTimeout;
    private Runnable mTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        GalaxySdk.sdkInitialize(this);
        GalaxySdk.setDebugEnabled(true);

        String token = Utils.loadAccessToken(this);

        if (TextUtils.isEmpty(token) || !Utils.isNetworkAvailable(this)) {
            mTask = new Runnable() {
                @Override
                public void run() {
                    showLogin();
                }
            };
        } else {
            GalaxyService service = new GalaxyService();
            service.setAccessToken(token);
            service.getProfile(new GalaxyCallback<Profile>() {
                @Override
                public void onSuccess(final Profile profile) {
                    synchronized (mLock) {
                        mTask = new Runnable() {
                            @Override
                            public void run() {
                                showMainView();
                            }
                        };
                        if (mTimeout) mTask.run();
                    }
                }

                @Override
                public void onError(final GalaxyRequestError error) {
                    synchronized (mLock) {
                        mTask = new Runnable() {
                            @Override
                            public void run() {
                                showLogin();
                            }
                        };
                        if (mTimeout) mTask.run();
                    }
                }
            });
        }

        //show splash screen at least MIN_DELAY milliseconds
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                synchronized (mLock) {
                    mTimeout = true;
                    if (mTask != null) {
                        mTask.run();
                    }
                }
            }
        }, MIN_DELAY);
    }

    private void showLogin() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Utils.removeAccessToken(SplashScreenActivity.this);
                startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
            }
        });
    }

    private void showMainView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent main = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(main);
            }
        });
    }
}
