package com.uphave.galaxy;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.uphave.galaxy.fragments.FollowingFragment;
import com.uphave.galaxy.fragments.NewsFeedFragment;
import com.uphave.galaxy.fragments.ProfileFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @Bind(R.id.toolbar)
    Toolbar mToolbar;
    @Bind(R.id.tabs)
    TabLayout mTabLayout;
    @Bind(R.id.pager)
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        final TabLayout.Tab tabNewsFeed = mTabLayout.newTab();
        final TabLayout.Tab tabFollowing = mTabLayout.newTab();
        final TabLayout.Tab tabProfile = mTabLayout.newTab();

        tabNewsFeed.setIcon(R.drawable.ic_news_feed);
        tabFollowing.setIcon(R.drawable.ic_following);
        tabProfile.setIcon(R.drawable.ic_profile);

        tabNewsFeed.setTag(0);
        tabFollowing.setTag(1);
        tabProfile.setTag(2);

        mTabLayout.addTab(tabNewsFeed);
        mTabLayout.addTab(tabFollowing);
        mTabLayout.addTab(tabProfile);

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(final int position) {
                switch (position) {
                    case 0:
                        return NewsFeedFragment.newInstance();
                    case 1:
                        return FollowingFragment.newInstance();
                    case 2:
                        return ProfileFragment.newInstance();
                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return 3;
            }
        });

        mTabLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }
}
