package com.uphave.galaxy;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {
    private static final String KEY_TOKEN = "token";
    private static final String KEY_USER_ID = "userId";
    private static final String PREFERENCES_NAME = "PREFERENCES";
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM ''yy");
    private static SimpleDateFormat shortTimeFormat = new SimpleDateFormat("HH:mm");

    public static void saveUsername(Context context, String userId) {
        if (context == null || userId == null) {
            throw new NullPointerException();
        }
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(KEY_USER_ID, userId).apply();
    }

    public static void saveAccessToken(Context context, String token) {
        if (context == null || token == null) {
            throw new NullPointerException();
        }
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit().putString(KEY_TOKEN, token).apply();
    }

    public static String loadUserName(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getString(KEY_USER_ID, null);
    }

    public static String loadAccessToken(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getString(KEY_TOKEN, null);
    }

//   public static void removeUserId(Context context) {
//      if (context == null) {
//         throw new NullPointerException();
//      }
//      SharedPreferences preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
//      preferences.edit().remove(KEY_USER_ID).commit();
//   }

    public static void removeAccessToken(Context context) {
        if (context == null) {
            throw new NullPointerException();
        }
        SharedPreferences preferences = context.getApplicationContext().getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit().remove(KEY_TOKEN).apply();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void alert(final Activity activity, final String error) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(activity, R.style.AppTheme_Dialog)
                        .setTitle(R.string.notification)
                        .setCancelable(true)
                        .setMessage(error)
                        .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });
    }

    public static String format(Date date) {
        return simpleDateFormat.format(date);
    }

    public static String format(final Calendar dateTime) {
        try {
            int year = dateTime.get(Calendar.YEAR);
            int month = dateTime.get(Calendar.MONTH);
            int day = dateTime.get(Calendar.DAY_OF_MONTH);
            int hour = dateTime.get(Calendar.HOUR_OF_DAY);
            int minute = dateTime.get(Calendar.MINUTE);
            int second = dateTime.get(Calendar.SECOND);
            int zone = dateTime.get(Calendar.ZONE_OFFSET) / 60000;
            char sign = zone > 0 ? '+' : '-';
            int minuteOffset = zone % 60;
            int hourOffset = zone / 60;
            return String.format("%04d-%02d-%02d %02d:%02d:%02d %c%02d:%02d", year, month, day, hour, minute, second, sign, hourOffset, minuteOffset);
        } catch (Exception ignored) {
            return null;
        }
    }

    public static final String formatDuration(int duration) {
//      duration = duration / 1000;
        int hour = duration / 3600;
        int min = (duration % 3600) / 60;
//      int sec = duration % 60;
        return String.format("%dh%02d\'", hour, min);
    }

    public static void notNull(Object arg, String name) {
        if (arg == null) {
            throw new NullPointerException("Argument '" + name + "' cannot be null");
        }
    }


    public static String formatShortTime(final Date time) {
        return shortTimeFormat.format(time);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }
        //I added this to try to fix half hidden row
        totalHeight++;

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static boolean validateEmail(final String email) {
        if (TextUtils.isEmpty(email)) return false;
        return email.contains("@");
    }
}