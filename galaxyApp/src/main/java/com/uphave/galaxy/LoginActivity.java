package com.uphave.galaxy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.uphave.galaxy.model.AccessToken;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    @Bind(R.id.edt_email)
    TextInputLayout mEdtEmail;
    @Bind(R.id.edt_username)
    TextInputLayout mEdtUsername;
    @Bind(R.id.edt_password)
    TextInputLayout mEdtPassword;
    @Bind(R.id.edt_password_2)
    TextInputLayout mEdtPassword2;
    @Bind(R.id.btn_sign_in)
    Button mBtnSignIn;
    @Bind(R.id.sign_up)
    TextView mTxtSignUp;

    private boolean isSignUp, isLogin;
    private EditText mEmail, mUsername, mPassword, mPassword2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mEmail = mEdtEmail.getEditText();
        mUsername = mEdtUsername.getEditText();
        mPassword = mEdtPassword.getEditText();
        mPassword2 = mEdtPassword2.getEditText();

        String userName = Utils.loadUserName(this);

        if (userName != null) {
            mUsername.setText(userName);
        }

        TextView.OnEditorActionListener inputCompleted = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if ((id == R.id.input_password && !isSignUp) || (id == R.id.input_email && isSignUp) || id == EditorInfo.IME_NULL) {
                    attemptToLogin();
                    return true;
                }
                return false;
            }
        };

        mPassword.setOnEditorActionListener(inputCompleted);
        mEmail.setOnEditorActionListener(inputCompleted);
        mBtnSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptToLogin();
            }
        });
        mTxtSignUp.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(final View v) {
                switchLayout();
            }
        });
    }

    private void attemptToLogin() {
        if (isLogin) return;

        hideSoftKeyboard();

        mEdtUsername.setError(null);
        mEdtPassword.setError(null);
        mEdtPassword2.setError(null);
        mEdtEmail.setError(null);

        String username = mUsername.getText().toString();
        String password = mPassword.getText().toString();
        String password2 = mPassword2.getText().toString();
        String email = mEmail.getText().toString();

        View focusView = null;

        if (TextUtils.isEmpty(username)) {
            mEdtUsername.setErrorEnabled(true);
            mEdtUsername.setError(getString(R.string.invalid_username));
            focusView = mEdtUsername;
        } else if (isSignUp) {
            if (!password.equals(password2)) {
                mEdtPassword2.setErrorEnabled(true);
                mEdtPassword2.setError(getString(R.string.password_mismatch));
                focusView = mEdtPassword2;
            } else if (!Utils.validateEmail(email)) {
                mEdtEmail.setErrorEnabled(true);
                mEdtEmail.setError(getString(R.string.invalid_email));
                focusView = mEdtEmail;
            }
        }

        if (focusView != null) {
            focusView.requestFocus();
        } else if (!Utils.isNetworkAvailable(this)) {
            Utils.alert(this, getString(R.string.network_not_available));
        } else {
            Utils.saveUsername(this, username);

            hideSoftKeyboard();
            isLogin = true;

            final ProgressDialog progressDialog = new ProgressDialog(this, R.style.AppTheme_Dialog);
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.setTitle(isSignUp ? R.string.action_sign_up : R.string.action_sign_in);
            progressDialog.setMessage(getString(isSignUp ? R.string.sign_up_for_galaxy : R.string.sign_in_to_galaxy) + "...");
            progressDialog.show();

            GalaxyService service = new GalaxyService();
            GalaxyCallback<AccessToken> callback = new GalaxyCallback<AccessToken>() {
                @Override
                public void onSuccess(final AccessToken token) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isLogin = false;
                            progressDialog.dismiss();
                            Utils.saveAccessToken(LoginActivity.this, token.getAccessToken());
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        }
                    });
                }

                @Override
                public void onError(final GalaxyRequestError error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isLogin = false;
                            progressDialog.dismiss();
                            Utils.alert(LoginActivity.this, "Invalid account");
                        }
                    });
                }
            };

            if (isSignUp) {
                service.register(username, password, email, callback);
            } else {
                service.login(username, password, callback);
            }
        }
    }

    private void switchLayout() {
        if (isSignUp) {
            isSignUp = false;

            Animation animation = AnimationUtils.makeOutAnimation(this, true);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(final Animation animation) {

                }

                @Override
                public void onAnimationEnd(final Animation animation) {
                    mEdtEmail.setVisibility(View.GONE);
                    mEdtPassword2.setVisibility(View.GONE);
                    mTxtSignUp.setText(R.string.sign_up_for_galaxy);
                    mPassword.setImeActionLabel(getString(R.string.action_sign_in), R.id.input_password);
                    mBtnSignIn.setText(R.string.action_sign_in);
                }

                @Override
                public void onAnimationRepeat(final Animation animation) {

                }
            });
            mEdtEmail.startAnimation(animation);
            mEdtPassword2.startAnimation(animation);

        } else {
            isSignUp = true;

            Animation animation = AnimationUtils.makeInAnimation(this, false);

            mEdtPassword2.setVisibility(View.VISIBLE);
            mEdtPassword2.startAnimation(animation);
            mEdtEmail.setVisibility(View.VISIBLE);
            mEdtEmail.startAnimation(animation);
            mTxtSignUp.setText(R.string.sign_in_to_galaxy);
            mPassword.setImeActionLabel(getString(R.string.action_next), R.id.input_password);
            mBtnSignIn.setText(R.string.action_sign_up);
        }
    }

    private void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}