package com.uphave.galaxy;

import android.content.Context;
import android.os.AsyncTask;

import com.misfit.cloud.BuildConfig;
import com.uphave.galaxy.utils.Validate;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;

/**
 * This class allows some customization of Galaxy SDK behavior.
 */
@SuppressWarnings("unused")
public final class GalaxySdk {
    private static final String TAG = GalaxySdk.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static volatile Executor executor;
    private static volatile boolean isDebugEnabled;
    private static Context applicationContext;
    private static Boolean sdkInitialized = false;

    /**
     * This function initializes the Galaxy SDK, the behavior of Galaxy SDK functions are
     * undetermined if this function is not called. It should be called as early as possible.
     *
     * @param applicationContext The application context
     */
    public static synchronized void sdkInitialize(Context applicationContext) {
        sdkInitialize(applicationContext, null);
    }

    /**
     * This function initializes the Galaxy SDK, the behavior of Galaxy SDK functions are
     * undetermined if this function is not called. It should be called as early as possible.
     *
     * @param applicationContext The application context
     * @param callback           A callback called when initialize finishes. This will be called even if the
     *                           sdk is already initialized.
     */
    public static synchronized void sdkInitialize(Context applicationContext, final InitializeCallback callback) {
        if (sdkInitialized) {
            if (callback != null) {
                callback.onInitialized();
            }
            return;
        }

        Validate.notNull(applicationContext, "applicationContext");
        Validate.hasInternetPermissions(applicationContext, false);

        FutureTask<Void> initializedFutureTask =
                new FutureTask<>(new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        if (callback != null) {
                            callback.onInitialized();
                        }
                        return null;
                    }
                });
        getExecutor().execute(initializedFutureTask);
        sdkInitialized = true;
    }

    /**
     * Returns the Executor used by the SDK for background work.
     * <p/>
     * By default this uses AsyncTask Executor via reflection if the API level is high enough.
     * Otherwise this creates a new Executor with defaults similar to those used in AsyncTask.
     *
     * @return an Executor used by the SDK.  This will never be null.
     */
    public static Executor getExecutor() {
        synchronized (LOCK) {
            if (executor == null) {
                executor = AsyncTask.THREAD_POOL_EXECUTOR;
            }
        }
        return executor;
    }

    /**
     * Sets the Executor used by the SDK for background work.
     *
     * @param executor the Executor to use; must not be null.
     */
    public static void setExecutor(Executor executor) {
        Validate.notNull(executor, "executor");
        synchronized (LOCK) {
            GalaxySdk.executor = executor;
        }
    }

    /**
     * Indicates whether the Galaxy SDK has been initialized.
     *
     * @return true if initialized, false if not
     */
    public static synchronized boolean isInitialized() {
        return sdkInitialized;
    }

    /**
     * Indicates if we are in debug mode.
     */
    public static boolean isDebugEnabled() {
        return isDebugEnabled;
    }

    /**
     * Used to enable or disable logging, and other debug features. Default values is false.
     *
     * @param enabled Debug features (like logging) are enabled if true, disabled if false.
     */
    public static void setDebugEnabled(final boolean enabled) {
        isDebugEnabled = enabled;
    }

    /**
     * Gets the base Galaxy domain to use when making Web requests
     *
     * @return the Galaxy domain
     */
    public static String getGalaxyDomain() {
        return BuildConfig.DOMAIN;
    }

    /**
     * Returns the current version of the Galaxy SDK for Android as a string.
     *
     * @return the current version of the SDK
     */
    public static String getSdkVersion() {
        return BuildConfig.BUILD;
    }

    /**
     * The getter for the context of the current application.
     *
     * @return The context of the current application.
     */
    public static Context getApplicationContext() {
        Validate.sdkInitialized();
        return applicationContext;
    }

    /**
     * Callback passed to the sdkInitialize function.
     */
    public interface InitializeCallback {
        /**
         * Called when the sdk has been initialized.
         */
        void onInitialized();
    }
}
