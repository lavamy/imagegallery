package com.uphave.galaxy;

import android.os.AsyncTask;
import android.util.Log;

import com.uphave.galaxy.utils.Utils;

class GalaxyRequestAsyncTask extends AsyncTask<Void, Void, GalaxyResponse> {
    private static final String TAG = GalaxyRequestAsyncTask.class.getCanonicalName();
    private final GalaxyRequest mRequest;
    private Exception mException;

    public GalaxyRequestAsyncTask(GalaxyRequest request) {
        mRequest = request;
    }

    public Exception getException() {
        return mException;
    }

    @Override
    protected GalaxyResponse doInBackground(Void... params) {
        try {
            return mRequest.executeAndWait();
        } catch (Exception e) {
            mException = e;
            return null;
        }
    }

    @Override
    protected void onPreExecute() {
        mException = null;
        if (GalaxySdk.isDebugEnabled()) {
            Log.d(TAG, String.format("Execute:\n%s", mRequest));
        }
    }

    @Override
    protected void onPostExecute(GalaxyResponse result) {
        if (GalaxySdk.isDebugEnabled()) {
            if (mException != null) {
                Utils.log(String.format("Exception:\n%s", mException));
            }
        }
    }
}