package com.uphave.galaxy.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.uphave.galaxy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * The news feed.
 */
public class NewsFeed implements Parcelable {
    public static final Creator<NewsFeed> CREATOR = new Creator<NewsFeed>() {
        @Override
        public NewsFeed createFromParcel(Parcel in) {
            return new NewsFeed(in);
        }

        @Override
        public NewsFeed[] newArray(int size) {
            return new NewsFeed[size];
        }
    };

    private static final String KEY_USER = "username";
    private static final String KEY_DATE = "date";
    private static final String KEY_STATUS = "status";
    private static final String KEY_IMAGE = "image";

    private final String mUsername;
    private final Calendar mDate;
    private final String mStatus;
    private final String mImage;

    private NewsFeed(Parcel in) {
        mUsername = in.readString();
        String time = in.readString();
        mDate = Utils.parseDateTime(time);
        mStatus = in.readString();
        mImage = in.readString();
    }

    public NewsFeed(final JSONObject jsonObject) {
        mUsername = jsonObject.optString(KEY_USER);
        mDate = Utils.parseDateTime(jsonObject.optString(KEY_DATE));
        mStatus = jsonObject.optString(KEY_STATUS);
        mImage = jsonObject.optString(KEY_IMAGE);
    }

    public String getUsername() {
        return mUsername;
    }

    public Calendar getDate() {
        return mDate;
    }

    public String getStatus() {
        return mStatus;
    }

    public String getImage() {
        return mImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(mUsername);
        dest.writeString(Utils.formatDateTime(mDate));
        dest.writeString(mStatus);
        dest.writeString(mImage);
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USER, mUsername);
            jsonObject.put(KEY_DATE, Utils.formatDateTime(mDate));
            jsonObject.put(KEY_STATUS, mStatus);
            jsonObject.put(KEY_IMAGE, mImage);
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }
}