package com.uphave.galaxy.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The corresponding access token of a user.
 */
public class AccessToken implements Parcelable {
    public static final Creator<AccessToken> CREATOR = new Creator<AccessToken>() {
        @Override
        public AccessToken createFromParcel(Parcel in) {
            return new AccessToken(in);
        }

        @Override
        public AccessToken[] newArray(int size) {
            return new AccessToken[size];
        }
    };

    private static final String KEY_TOKEN = "access_token";

    private final String mAccessToken;

    private AccessToken(Parcel in) {
        mAccessToken = in.readString();
    }

    public AccessToken(final JSONObject jsonObject) {
        mAccessToken = jsonObject.optString(KEY_TOKEN);
    }

    /**
     * Get access token of user.
     *
     * @return Returns access token of user.
     */
    public String getAccessToken() {
        return mAccessToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(mAccessToken);
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_TOKEN, mAccessToken);
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }
}