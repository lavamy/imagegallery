package com.uphave.galaxy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.uphave.galaxy.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * The corresponding profile of a user.
 */
public class Profile implements Parcelable {
    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    private static final String KEY_USERNAME = "username";
    private static final String KEY_NAME = "name";
    private static final String KEY_AVATAR = "avatar";
    private static final String KEY_BIRTHDAY = "birthday";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_EMAIL = "email";
    private static final String GENDER_MALE = "male";
    private static final String GENDER_FEMALE = "female";

    private final String mUserId;
    private final String mName;
    private final String mAvatar;
    private final String mEmail;
    private final Date mBirthday;
    private final Gender mGender;

    private Profile(Parcel in) {
        mUserId = in.readString();
        mName = in.readString();
        mAvatar = in.readString();
        mEmail = in.readString();
        long date = in.readLong();
        if (date != -1)
            mBirthday = new Date(date);
        else
            mBirthday = null;
        mGender = Gender.valueOf(in.readString());
    }

    public Profile(final JSONObject jsonObject) {
        mUserId = jsonObject.optString(KEY_USERNAME);
        mName = jsonObject.optString(KEY_NAME);
        mAvatar = jsonObject.optString(KEY_AVATAR);
        mEmail = jsonObject.optString(KEY_EMAIL);
        Date birthday = null;
        try {
            birthday = Utils.parseDate(jsonObject.optString(KEY_BIRTHDAY));
        } catch (Exception ignored) {
        }
        mBirthday = birthday;
        String genderStr = jsonObject.optString(KEY_GENDER);
        if (genderStr != null) {
            switch (genderStr) {
                case GENDER_MALE:
                    mGender = Gender.MALE;
                    break;
                case GENDER_FEMALE:
                    mGender = Gender.FEMALE;
                    break;
                default:
                    mGender = Gender.UNKNOWN;
                    break;
            }
        } else {
            mGender = Gender.UNKNOWN;
        }
    }

    /**
     * Get name of user.
     *
     * @return Returns name of user.
     */
    public String getName() {
        return mName;
    }

    /**
     * Get URL string of user's avatar.
     *
     * @return Returns URL string of user's avatar.
     */
    public String getAvatar() {
        return mAvatar;
    }

    /**
     * Get gender of user.
     *
     * @return Returns gender of user.
     */
    public Gender getGender() {
        return mGender;
    }

    /**
     * Get birthday of user.
     *
     * @return Returns birthday of user.
     */
    public Date getBirthday() {
        return mBirthday;
    }

    /**
     * Get email of user.
     *
     * @return Returns email of user.
     */
    public String getEmail() {
        return mEmail;
    }

    /**
     * Get id of user.
     *
     * @return Returns id of user.
     */
    public String getUserId() {
        return mUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(mUserId);
        dest.writeString(mName);
        dest.writeString(mAvatar);
        dest.writeString(mEmail);
        if (mBirthday != null)
            dest.writeLong(mBirthday.getTime());
        else
            dest.writeLong(-1);
        dest.writeString(mGender.name());
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(KEY_USERNAME, mUserId);
            jsonObject.put(KEY_NAME, mName);
            jsonObject.put(KEY_AVATAR, mAvatar);
            jsonObject.put(KEY_EMAIL, mEmail);
            jsonObject.put(KEY_BIRTHDAY, Utils.formatDate(mBirthday));
            jsonObject.put(KEY_GENDER, mGender);
            jsonObject.put(KEY_EMAIL, mEmail);
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }

    /**
     * Represents gender of user.
     */
    public enum Gender {
        MALE,
        FEMALE,
        UNKNOWN
    }
}