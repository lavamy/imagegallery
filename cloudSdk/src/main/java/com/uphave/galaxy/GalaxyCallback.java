package com.uphave.galaxy;

/**
 * A callback class for the Galaxy SDK.
 */
public interface GalaxyCallback<RESULT> {
    /**
     * Called when the request completes without error.
     *
     * @param result Result from the Galaxy server
     */
    void onSuccess(RESULT result);

    /**
     * Called when the request finishes with an error.
     *
     * @param error The error that occurred
     */
    void onError(GalaxyRequestError error);
}