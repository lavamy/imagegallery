package com.uphave.galaxy;

import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.uphave.galaxy.exception.GalaxyException;
import com.uphave.galaxy.utils.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.Set;

/**
 * A single request to be sent to the Galaxy Platform through the Galaxy API.
 */
public class GalaxyRequest {
    private static final String ACCEPT_LANGUAGE_HEADER = "Accept-Language";
    private static final String TAG = GalaxyRequest.class.getSimpleName();

    private HttpMethod mMethod;
    private String mPath;
    private Bundle mHeaderParams;
    private Bundle mUrlParams;
    private Callback mCallback;
    private int mTimeout;
    private byte[] mData;

    /**
     * Constructs a request with a specific HTTP method, mPath and parameters.
     *
     * @param path         the request path to retrieve, create, or delete
     * @param headerParams additional params to pass along with the Galaxy API request in header.
     * @param urlParams    additional params to pass along with the Galaxy API request in URL.
     * @param mMethod      the {@link HttpMethod} to use for the request, or null for default
     *                     (HttpMethod.GET)
     * @param callback     a mCallback that will be called when the request is completed to handle
     *                     success or error conditions
     */
    public GalaxyRequest(String path, Bundle headerParams, Bundle urlParams, HttpMethod mMethod, Callback callback) {
        this(path, headerParams, urlParams, mMethod, callback, 0);
    }

    /**
     * Constructs a request with a specific HTTP method, path and parameters.
     *
     * @param path         the request path to retrieve, create, or delete
     * @param headerParams additional params to pass along with the Galaxy API request in header.
     * @param urlParams    additional params to pass along with the Galaxy API request in URL.
     * @param mMethod      the {@link HttpMethod} to use for the request, or null for default
     *                     (HttpMethod.GET)
     * @param callback     a mCallback that will be called when the request is completed to handle
     *                     success or error conditions
     * @param timeout      mTimeout in milliseconds
     */
    public GalaxyRequest(String path, Bundle headerParams, Bundle urlParams, HttpMethod mMethod, Callback callback, int timeout) {
        this(path, headerParams, urlParams, mMethod, callback, timeout, null);
    }

    public GalaxyRequest(String path, Bundle headerParams, Bundle urlParams, HttpMethod mMethod, Callback callback, int timeout, byte[] data) {
        setPath(path);
        setHeaderParams(headerParams);
        setUrlParams(urlParams);
        setMethod(mMethod);
        setCallback(callback);
        setTimeout(timeout);
        setData(data);
    }

    private static HttpURLConnection toHttpConnection(GalaxyRequest request) {
        URL url;
        try {
            url = new URL(buildRequestUrl(request));
            if (GalaxySdk.isDebugEnabled()) {
                Log.i(TAG, "Generated URL: " + url.toString());
            }
        } catch (MalformedURLException e) {
            throw new GalaxyException("Could not construct URL for request", e);
        }

        HttpURLConnection connection;
        try {
            connection = createConnection(url, request);
        } catch (IOException e) {
            throw new GalaxyException("Could not construct request body", e);
        }
        return connection;
    }

    private static String buildRequestUrl(GalaxyRequest request) {
        Uri.Builder uriBuilder = new Uri.Builder();
        if (!request.mPath.startsWith(GalaxySdk.getGalaxyDomain())) {
            uriBuilder = uriBuilder.encodedPath(GalaxySdk.getGalaxyDomain());
            uriBuilder = uriBuilder.appendEncodedPath(request.mPath);
        } else {
            uriBuilder = uriBuilder.encodedPath(request.mPath);
        }

        if (request.mUrlParams != null) {
            Set<String> keys = request.mUrlParams.keySet();
            for (String key : keys) {
                Object value = request.mUrlParams.get(key);
                uriBuilder.appendQueryParameter(key, parameterToString(value));
            }
        }
        return uriBuilder.toString();
    }

    private static boolean isSupportedParameterType(Object value) {
        return value instanceof String || value instanceof Boolean || value instanceof Number;
    }

    private static String parameterToString(Object value) {
        if (value == null) {
            value = "";
        }
        if (isSupportedParameterType(value)) {
            if (value instanceof String) {
                return (String) value;
            } else if (value instanceof Boolean || value instanceof Number) {
                return value.toString();
            }
        }
        throw new IllegalArgumentException(String.format("Unsupported parameter type for request: %s", value.getClass().getSimpleName()));
    }

    private static HttpURLConnection createConnection(URL url, GalaxyRequest request) throws IOException {
        HttpURLConnection connection;
        connection = (HttpURLConnection) url.openConnection();
        connection.setChunkedStreamingMode(0);
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.setRequestMethod(request.mMethod.name());
        connection.setConnectTimeout(request.mTimeout);
        connection.setReadTimeout(request.mTimeout);
        connection.setRequestProperty(ACCEPT_LANGUAGE_HEADER, Locale.US.toString());
        connection.setRequestProperty("Content-Type", "application/json");

        connection.setDoInput(true);

        if (request.mHeaderParams != null) {
            Set<String> keySet = request.mHeaderParams.keySet();
            for (String key : keySet) {
                connection.setRequestProperty(key, parameterToString(request.mHeaderParams.get(key)));
            }
        }
        if (request.mData != null) {
            String boundary = "---011000010111000001101001";
            String crlf = "\r\n";

            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Cache-Control", "no-cache");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
            connection.setRequestProperty("Accept-Language", "en-us,en;q=0.5");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:12.0) Gecko/20100101 Firefox/12.0");
            connection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
            connection.setRequestProperty("Accept-Encoding", "gzip, deflate");

            connection.setDoOutput(true);
            DataOutputStream output = new DataOutputStream(connection.getOutputStream());

            output.writeBytes("--" + boundary + crlf);
            output.writeBytes("Content-Disposition: form-data;name=\"image\"" + crlf + crlf);

            output.write(Base64.encode(request.mData, Base64.NO_WRAP));
            output.writeBytes(crlf);
            output.writeBytes("--" + boundary + "--" + crlf);

            output.flush();
            output.close();
        }

        return connection;
    }

    /**
     * Returns the path of this request, if any.
     *
     * @return the path of this request
     */
    public final String getPath() {
        return this.mPath;
    }

    /**
     * Sets the path of this request.
     *
     * @param path the path for this request
     */
    public final void setPath(String path) {
        Validate.notNullOrEmpty(path, "path");
        this.mPath = path;
    }

    /**
     * Returns the {@link HttpMethod} to use for this request.
     *
     * @return the HttpMethod
     */
    public final HttpMethod getMethod() {
        return this.mMethod;
    }

    /**
     * Sets the {@link HttpMethod} to use for this request.
     *
     * @param method the HttpMethod, or null for the default (HttpMethod.GET).
     */
    public final void setMethod(HttpMethod method) {
        this.mMethod = (method != null) ? method : HttpMethod.GET;
    }

    /**
     * Returns the header params for this request.
     *
     * @return the header params
     */
    public final Bundle getHeaderParams() {
        return this.mHeaderParams;
    }

    /**
     * Sets the header params for this request.
     *
     * @param headerParams the header params
     */
    public final void setHeaderParams(Bundle headerParams) {
        this.mHeaderParams = headerParams;
    }

    /**
     * Returns the callback which will be called when the request finishes.
     *
     * @return the callback
     */
    public final Callback getCallback() {
        return mCallback;
    }

    /**
     * Sets the callback which will be called when the request finishes.
     *
     * @param callback the callback
     */
    public final void setCallback(final Callback callback) {
        Validate.notNull(callback, "callback");
        if (GalaxySdk.isDebugEnabled()) {
            this.mCallback = new Callback() {
                @Override
                public void onCompleted(final GalaxyResponse response) {
                    Log.d(TAG, String.format("Result:\n%s", response));
                    callback.onCompleted(response);
                }
            };
        } else {
            this.mCallback = callback;
        }
    }

    /**
     * Gets the timeout to wait for responses from the server before a timeout error occurs.
     *
     * @return the timeout, in milliseconds; 0 (the default) means do not timeout
     */
    public int getTimeout() {
        return mTimeout;
    }

    /**
     * Sets the timeout to wait for responses from the server before a timeout error occurs.
     *
     * @param timeoutInMilliseconds the timeout, in milliseconds; 0 means do not timeout
     */
    public void setTimeout(int timeoutInMilliseconds) {
        if (timeoutInMilliseconds < 0) {
            throw new IllegalArgumentException("Argument timeout must be >= 0.");
        }
        this.mTimeout = timeoutInMilliseconds;
    }

    /**
     * Returns the URL params for this request.
     *
     * @return the URL params
     */
    public Bundle getUrlParams() {
        return mUrlParams;
    }

    /**
     * Sets the URL params for this request.
     *
     * @param urlParams the URL params
     */
    public void setUrlParams(Bundle urlParams) {
        if (urlParams != null) {
            this.mUrlParams = new Bundle(urlParams);
        } else {
            this.mUrlParams = new Bundle();
        }
    }

    /**
     * Executes this request on the current thread and blocks while waiting for the response.
     * <p/>
     * This should only be called if you have transitioned off the UI thread.
     *
     * @return the Response object representing the results of the request
     * @throws GalaxyException          If there was an error in the protocol used to communicate
     *                                  with the service
     * @throws IllegalArgumentException
     */
    public final GalaxyResponse executeAndWait() {
        HttpURLConnection connection;
        try {
            connection = toHttpConnection(this);
        } catch (Exception ex) {
            GalaxyResponse response = GalaxyResponse.constructErrorResponse(this, new GalaxyException(ex));
            this.mCallback.onCompleted(response);
            return response;
        }
        GalaxyResponse response = GalaxyResponse.fromHttpConnection(connection, this);
        connection.disconnect();
        this.mCallback.onCompleted(response);
        return response;
    }

    /**
     * Executes the request asynchronously. This function will return immediately,
     * and the request will be processed on a separate thread. In order to process result of a
     * request, or determine whether a request succeeded or failed, a mCallback must be specified
     * (see the {@link #setCallback(Callback) setCallback} method).
     * <p/>
     * This should only be called from the UI thread.
     *
     * @throws IllegalArgumentException
     */
    public final void executeAsync() {
        GalaxyRequestAsyncTask asyncTask = new GalaxyRequestAsyncTask(this);
        asyncTask.executeOnExecutor(GalaxySdk.getExecutor());
    }

    /**
     * Returns a string representation of this Request, useful for debugging.
     *
     * @return the debugging information
     */
    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("method", mMethod);
            jsonObject.put("path", mPath);
            jsonObject.put("headerParams", mHeaderParams);
            jsonObject.put("urlParams", mUrlParams);
            jsonObject.put("timeout", mTimeout);
            jsonObject.put("callback", mCallback);
            jsonObject.put("data", mData == null ? "null" : mData.length);
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }

    public byte[] getData() {
        return mData;
    }

    public void setData(byte[] data) {
        mData = data;
    }

    /**
     * Specifies the interface that consumers of the Request class can implement in order to be
     * notified when a particular request completes, either successfully or with an error.
     */
    public interface Callback {
        /**
         * The method that will be called when a request completes.
         *
         * @param response the Response of this request, which may include error information if the
         *                 request was unsuccessful
         */
        void onCompleted(GalaxyResponse response);
    }
}