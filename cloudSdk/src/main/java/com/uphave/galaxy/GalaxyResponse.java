package com.uphave.galaxy;

import com.uphave.galaxy.exception.GalaxyException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/**
 * Encapsulates the response, successful or otherwise, of a call to the Galaxy platform.
 */
public class GalaxyResponse {
    private final GalaxyRequest request;
    private final JSONObject returnedObject;
    private final GalaxyRequestError error;

    GalaxyResponse(GalaxyRequest request, JSONObject returnedObject) {
        this(request, returnedObject, null);
    }

    GalaxyResponse(GalaxyRequest request, JSONObject returnedObject, GalaxyRequestError error) {
        this.request = request;
        this.returnedObject = returnedObject;
        this.error = error;
    }

    GalaxyResponse(GalaxyRequest request, GalaxyRequestError error) {
        this(request, null, error);
    }

    static GalaxyResponse fromHttpConnection(HttpURLConnection connection, GalaxyRequest request) {
        InputStream stream = null;
        try {
            int responseCode = connection.getResponseCode();
            if (responseCode == 200) {
                stream = connection.getInputStream();
                String responseString = readStreamToString(stream);
                JSONTokener jsonTokener = new JSONTokener(responseString);
                Object object = jsonTokener.nextValue();
                if (object instanceof JSONObject) {
                    return new GalaxyResponse(request, (JSONObject) object);
                } else {
                    try {
                        return new GalaxyResponse(request, new JSONObject().put("data", object));
                    } catch (JSONException e) {
                        e.printStackTrace();
                        throw new GalaxyException("Got unexpected object type in response, class: " + object.getClass().getSimpleName());
                    }
                }
            } else {
                try {
                    stream = connection.getInputStream();
                    String responseString = readStreamToString(stream);
                    JSONTokener jsonTokener = new JSONTokener(responseString);
                    Object object = jsonTokener.nextValue();
                    if (object instanceof JSONObject) {
                        return constructErrorResponse(request, new GalaxyRequestError(responseCode, ((JSONObject) object).getString("message")));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return constructErrorResponse(request, new GalaxyRequestError(responseCode, connection.getResponseMessage()));
            }
        } catch (GalaxyException ex) {
            return constructErrorResponse(request, ex);
        } catch (JSONException exception) {
            return constructErrorResponse(request, new GalaxyException(exception));
        } catch (IOException exception) {
            return constructErrorResponse(request, new GalaxyException(exception));
        } catch (SecurityException exception) {
            return constructErrorResponse(request, new GalaxyException(exception));
        } finally {
            try {
                if (stream != null)
                    stream.close();
            } catch (IOException ignored) {
            }
        }
    }

    private static String readStreamToString(InputStream inputStream) throws IOException {
        BufferedInputStream bufferedInputStream = null;
        InputStreamReader reader = null;
        //noinspection TryFinallyCanBeTryWithResources
        try {
            bufferedInputStream = new BufferedInputStream(inputStream);
            reader = new InputStreamReader(bufferedInputStream);
            StringBuilder stringBuilder = new StringBuilder();

            final int bufferSize = 1024 * 2;
            char[] buffer = new char[bufferSize];
            int n;
            while ((n = reader.read(buffer)) != -1) {
                stringBuilder.append(buffer, 0, n);
            }

            return stringBuilder.toString();
        } finally {
            assert bufferedInputStream != null;
            bufferedInputStream.close();
            assert reader != null;
            reader.close();
        }
    }

    static GalaxyResponse constructErrorResponse(GalaxyRequest request, GalaxyRequestError error) {
        return new GalaxyResponse(request, error);
    }

    static GalaxyResponse constructErrorResponse(GalaxyRequest request, GalaxyException error) {
        return new GalaxyResponse(request, new GalaxyRequestError(error));
    }

    /**
     * Returns information about any errors that may have occurred during the request.
     *
     * @return the error from the server, or null if there was no server error
     */
    public final GalaxyRequestError getError() {
        return error;
    }

    /**
     * The response returned for this request, if it's in object form.
     *
     * @return the returned JSON object, or null if none was returned
     */
    public final JSONObject getJSONObject() {
        return returnedObject;
    }

    /**
     * Returns the request that this response is for.
     *
     * @return the request that this response is for
     */
    public GalaxyRequest getRequest() {
        return request;
    }

    /**
     * Provides a debugging string for this response.
     */
    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            if (returnedObject != null)
                jsonObject.put("returnedObject", returnedObject);
            if (error != null)
                jsonObject.put("error", new JSONObject(error.toString()));
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }


}
