package com.uphave.galaxy;

import android.os.Bundle;

import com.uphave.galaxy.model.AccessToken;
import com.uphave.galaxy.model.NewsFeed;
import com.uphave.galaxy.model.Profile;
import com.uphave.galaxy.utils.Utils;
import com.uphave.galaxy.utils.Validate;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class is a high-level wrapper to send Galaxy request and handle responses from remote server.
 */
public final class GalaxyService {
    private static final String REQUEST_PROFILE_GET = "api/profile/";
    private static final String REQUEST_PROFILE_REGISTER = "api/profile/register/";
    private static final String REQUEST_PROFILE_LOGIN = "api/profile/login";
    private static final String REQUEST_PROFILE_UPDATE = "api/profile/update";

    private static final String REQUEST_ACTIVITY_FOLLOW = "api/activities/follow/%s";
    private static final String REQUEST_ACTIVITY_UNFOLLOW = "api/activities/unfollow/%s";
    private static final String REQUEST_ACTIVITY_FOLLOWING = "api/activities/following/";

    private static final String REQUEST_GALLERY_UPLOAD = "api/gallery/upload";
    private static final String REQUEST_GALLERY_BROWSER = "api/gallery/browser/%s";

    private static final String KEY_ACCESS_TOKEN = "access_token";

    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_FOLLOWING = "following";

    private static final String KEY_QUERY_START_DATE = "start_date";
    private static final String KEY_QUERY_END_DATE = "end_date";

    private static final String KEY_NEWS_FEED = "data";
    private static final String KEY_STATUS = "status";

    private String mAccessToken;

    public GalaxyService() {
    }

    /**
     * Get the current access token
     *
     * @return Returns the current access token.
     */
    public String getAccessToken() {
        return mAccessToken;
    }

    /**
     * Set a new access token that will be passed along with the Galaxy API request.
     *
     * @param accessToken the new access token
     */
    public void setAccessToken(final String accessToken) {
        Validate.notNullOrEmpty(accessToken, "access token");
        this.mAccessToken = accessToken;
    }


    /**
     * Send a request getting current user's profile to Galaxy server.
     *
     * @param callback the callback
     */
    public void getProfile(final GalaxyCallback<Profile> callback) {
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new Profile(jsonObject));
                }
            }
        };
        GalaxyRequest request = new GalaxyRequest(REQUEST_PROFILE_GET, getCommonHeaderParams(), null, HttpMethod.GET, completedCallback);
        request.executeAsync();
    }

    private Bundle getCommonHeaderParams() {
        Bundle headerParams = new Bundle();
        headerParams.putString(KEY_ACCESS_TOKEN, mAccessToken);
        return headerParams;
    }

    /**
     * Send a registration request to Galaxy server.
     *
     * @param callback the callback
     */
    public void register(String username, String password, String email, final GalaxyCallback<AccessToken> callback) {
        Validate.notNullOrEmpty(username, "username");
        Validate.notNull(password, "password");
        Validate.notNullOrEmpty(email, "email");
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new AccessToken(jsonObject));
                }
            }
        };
        Bundle headers = new Bundle();
        headers.putString(KEY_USERNAME, username);
        headers.putString(KEY_PASSWORD, password);
        headers.putString(KEY_EMAIL, email);
        GalaxyRequest request = new GalaxyRequest(REQUEST_PROFILE_REGISTER, headers, null, HttpMethod.POST, completedCallback);
        request.executeAsync();
    }

    /**
     * Send a updating request to Galaxy server.
     *
     * @param callback the callback
     */
    public void update(final String username, final String password, final String email, final GalaxyCallback<Profile> callback) {
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new Profile(jsonObject));
                }
            }
        };
        Bundle headers = getCommonHeaderParams();
        headers.putString(KEY_USERNAME, username);
        headers.putString(KEY_PASSWORD, password);
        headers.putString(KEY_EMAIL, email);
        GalaxyRequest request = new GalaxyRequest(REQUEST_PROFILE_UPDATE, headers, null, HttpMethod.PUT, completedCallback);
        request.executeAsync();
    }

    /**
     * Send a login request to Galaxy server.
     *
     * @param callback the callback
     */
    public void login(String username, String password, final GalaxyCallback<AccessToken> callback) {
        Validate.notNullOrEmpty(username, "username");
        Validate.notNull(password, "password");
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new AccessToken(jsonObject));
                }
            }
        };
        Bundle headers = new Bundle();
        headers.putString(KEY_USERNAME, username);
        headers.putString(KEY_PASSWORD, password);
        GalaxyRequest request = new GalaxyRequest(REQUEST_PROFILE_LOGIN, headers, null, HttpMethod.POST, completedCallback);
        request.executeAsync();
    }

    /**
     * Send a request getting the list of following users to Galaxy server.
     *
     * @param callback the callback
     */
    public void getFollowingUsers(final GalaxyCallback<List<Profile>> callback) {
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    JSONArray details = jsonObject.optJSONArray(KEY_FOLLOWING);
                    List<Profile> result = new ArrayList<>();
                    if (details != null) {
                        for (int i = 0; i < details.length(); i++) {
                            JSONObject detail = details.optJSONObject(i);
                            if (detail != null && detail != JSONObject.NULL) {
                                result.add(new Profile(detail));
                            }
                        }
                    }
                    callback.onSuccess(result);
                }
            }
        };
        GalaxyRequest request = new GalaxyRequest(REQUEST_ACTIVITY_FOLLOWING, getCommonHeaderParams(), null, HttpMethod.GET, completedCallback);
        request.executeAsync();
    }

    /**
     * Send a request following a user to Galaxy server.
     *
     * @param callback the callback
     */
    public void follow(String username, final GalaxyCallback<Profile> callback) {
        Validate.notNullOrEmpty(username, "username");
        Validate.notNull(callback, "callback");

        String requestPath = String.format(REQUEST_ACTIVITY_FOLLOW, username);

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new Profile(jsonObject));
                }
            }
        };
        GalaxyRequest request = new GalaxyRequest(requestPath, getCommonHeaderParams(), null, HttpMethod.GET, completedCallback);
        request.executeAsync();
    }

    /**
     * Send a request unfollowing a user to Galaxy server.
     *
     * @param callback the callback
     */
    public void unfollow(String username, final GalaxyCallback<Profile> callback) {
        Validate.notNullOrEmpty(username, "username");
        Validate.notNull(callback, "callback");

        String requestPath = String.format(REQUEST_ACTIVITY_UNFOLLOW, username);

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new Profile(jsonObject));
                }
            }
        };
        GalaxyRequest request = new GalaxyRequest(requestPath, getCommonHeaderParams(), null, HttpMethod.GET, completedCallback);
        request.executeAsync();
    }

    public void browser(Date startDate, Date endDate, final GalaxyCallback<List<NewsFeed>> callback) {
        Validate.notNull(startDate, "startDate");
        Validate.notNull(endDate, "endDate");
        Validate.notNull(callback, "callback");
        browserImp("", startDate, endDate, callback);
    }

    public void browser(String username, Date startDate, Date endDate, final GalaxyCallback<List<NewsFeed>> callback) {
        Validate.notNullOrEmpty(username, "username");
        Validate.notNull(startDate, "startDate");
        Validate.notNull(endDate, "endDate");
        Validate.notNull(callback, "callback");

        browserImp(username, startDate, endDate, callback);
    }

    private void browserImp(String username, Date startDate, Date endDate, final GalaxyCallback<List<NewsFeed>> callback) {
        final String requestPath = String.format(REQUEST_GALLERY_BROWSER, username);

//      Bundle urlParams = new Bundle();
//      urlParams.putString(KEY_QUERY_START_DATE, Utils.formatDate(startDate));
//      urlParams.putString(KEY_QUERY_END_DATE, Utils.formatDate(endDate));
//
        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    JSONArray feeds = jsonObject.optJSONArray(KEY_NEWS_FEED);
                    List<NewsFeed> result = new ArrayList<>();
                    if (feeds != null) {
                        for (int i = 0; i < feeds.length(); i++) {
                            JSONObject feed = feeds.optJSONObject(i);
                            if (feed != null && feed != JSONObject.NULL) {
                                result.add(new NewsFeed(feed));
                            }
                        }
                    }
                    callback.onSuccess(result);
                }
            }
        };

        Bundle header = getCommonHeaderParams();
        header.putString(KEY_QUERY_START_DATE, Utils.formatDate(startDate));
        header.putString(KEY_QUERY_END_DATE, Utils.formatDate(endDate));
        header.putString(KEY_USERNAME, "" + username);
        GalaxyRequest request = new GalaxyRequest(requestPath, header, null, HttpMethod.GET, completedCallback);
        request.executeAsync();
    }

    public void upload(byte[] image, String status, final GalaxyCallback<NewsFeed> callback) {
        Validate.notNull(image, "image");
        Validate.notNullOrEmpty(status, "status");
        Validate.notNull(callback, "callback");

        GalaxyRequest.Callback completedCallback = new GalaxyRequest.Callback() {
            @Override
            public void onCompleted(final GalaxyResponse response) {
                JSONObject jsonObject = response.getJSONObject();
                if (jsonObject == null) {
                    callback.onError(response.getError());
                } else {
                    callback.onSuccess(new NewsFeed(jsonObject));
                }
            }
        };
        Bundle headers = getCommonHeaderParams();
        headers.putString(KEY_STATUS, status);
        GalaxyRequest request = new GalaxyRequest(REQUEST_GALLERY_UPLOAD, headers, null, HttpMethod.POST, completedCallback, 0, image);
        request.executeAsync();
    }

}
