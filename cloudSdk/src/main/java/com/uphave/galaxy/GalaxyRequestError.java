package com.uphave.galaxy;

import com.uphave.galaxy.exception.GalaxyException;
import com.uphave.galaxy.exception.GalaxyServiceException;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class represents an error that occurred during a Galaxy request.
 */
public final class GalaxyRequestError {
    /**
     * Error code represents for local exception.
     */
    public static final int LOCAL_ERROR = -1;

    private final int errorCode;
    private final String errorMessage;
    private final GalaxyException exception;

    GalaxyRequestError(Exception exception) {
        this(LOCAL_ERROR, null, new GalaxyException(exception));
    }

    private GalaxyRequestError(int errorCode, String errorMessage, GalaxyException exception) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;

        if (exception != null) {
            this.exception = exception;
        } else {
            this.exception = new GalaxyServiceException(errorMessage);
        }
    }

    GalaxyRequestError(int errorCode, String errorMessage) {
        this(errorCode, errorMessage, null);
    }

    /**
     * Returns the error code returned from Galaxy.
     *
     * @return the error code returned from Galaxy
     */
    public int getErrorCode() {
        return errorCode;
    }

    /**
     * Returns the error message returned from Galaxy.
     *
     * @return the error message returned from Galaxy
     */
    public String getErrorMessage() {
        if (errorMessage != null) {
            return errorMessage;
        } else {
            return exception.getLocalizedMessage();
        }
    }

    /**
     * Returns the exception associated with this request, if any.
     *
     * @return the exception associated with this request
     */
    public GalaxyException getException() {
        return exception;
    }

    @Override
    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errorCode", errorCode);
            jsonObject.put("errorMessage", errorMessage);
            jsonObject.put("exception", exception);
            return jsonObject.toString(4);
        } catch (JSONException ignored) {
            return "{}";
        }
    }
}