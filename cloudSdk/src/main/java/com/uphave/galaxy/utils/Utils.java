package com.uphave.galaxy.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    private static final String ISO_8601_SHORT_FORMAT_STRING = "yyyy-MM-dd";

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat(ISO_8601_SHORT_FORMAT_STRING, Locale.US);

    public static String formatDate(Date date) {
        try {
            return dateFormat.format(date);
        } catch (Exception ignored) {
            return null;
        }
    }

    public static String formatDateTime(Calendar dateTime) {
        try {
            int year = dateTime.get(Calendar.YEAR);
            int month = dateTime.get(Calendar.MONTH);
            int day = dateTime.get(Calendar.DAY_OF_MONTH);
            int hour = dateTime.get(Calendar.HOUR_OF_DAY);
            int minute = dateTime.get(Calendar.MINUTE);
            int second = dateTime.get(Calendar.SECOND);
            int zone = dateTime.get(Calendar.ZONE_OFFSET) / 60000;
            char sign = zone > 0 ? '+' : '-';
            int minuteOffset = zone % 60;
            int hourOffset = zone / 60;
            return String.format("%04d-%02d-%02dT%02d:%02d:%02d%c%02d:%02d", year, month, day, hour, minute, second, sign, hourOffset, minuteOffset);
        } catch (Exception ignored) {
            return null;
        }
    }

    public static Date parseDate(String date) {
        try {
            return dateFormat.parse(date);
        } catch (Exception ignored) {
            return null;
        }
    }


    public static Calendar parseDateTime(String dateTime) {
        try {
            String regex = "(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2})\\:(\\d{2})\\:(\\d{2}).(\\d{7})([+-])(\\d{2})\\:(\\d{2})";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(dateTime);
            if (m.find()) {
                MatchResult mr = m.toMatchResult();
                int year = Integer.valueOf(mr.group(1));
                int month = Integer.valueOf(mr.group(2));
                int day = Integer.valueOf(mr.group(3));
                int hour = Integer.valueOf(mr.group(4));
                int minute = Integer.valueOf(mr.group(5));
                int second = Integer.valueOf(mr.group(6));
                String sign = mr.group(8);
                String hourOffset = mr.group(9);
                String minuteOffset = mr.group(10);
                String timezone = String.format("GMT%s%s:%s", sign, hourOffset, minuteOffset);
                TimeZone zone = TimeZone.getTimeZone(timezone);
                Calendar calendar = Calendar.getInstance(zone);
                calendar.set(year, month, day, hour, minute, second);
                return calendar;
            }
        } catch (Exception ignored) {
        }
        return null;
    }

    public static void log(String veryLongString) {
        int maxLogSize = 1000;
        for (int i = 0; i <= veryLongString.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > veryLongString.length() ? veryLongString.length() : end;
            Log.v("DB", veryLongString.substring(start, end));
        }
    }

}
