package com.uphave.galaxy.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import com.uphave.galaxy.GalaxySdk;
import com.uphave.galaxy.exception.GalaxySdkNotInitializedException;

public final class Validate {

    private static final String TAG = Validate.class.getName();

    private static final String NO_INTERNET_PERMISSION_REASON =
            "No internet permissions granted for the app, please add " +
                    "<uses-permission android:name=\"android.permission.INTERNET\" /> " +
                    "to your AndroidManifest.xml.";

    public static void notNullOrEmpty(String arg, String name) {
        if (isEmpty(arg)) {
            throw new IllegalArgumentException("Argument '" + name + "' cannot be null or empty");
        }
    }

    public static boolean isEmpty(final String arg) {
        return arg == null || arg.isEmpty();
    }

    public static void sdkInitialized() {
        if (!GalaxySdk.isInitialized()) {
            throw new GalaxySdkNotInitializedException("The SDK has not been initialized, make sure to call GalaxySdk.sdkInitialize() first.");
        }
    }

    public static void hasInternetPermissions(Context context, boolean shouldThrow) {
        Validate.notNull(context, "context");
        if (context.checkCallingOrSelfPermission(Manifest.permission.INTERNET) == PackageManager.PERMISSION_DENIED) {
            if (shouldThrow) {
                throw new IllegalStateException(NO_INTERNET_PERMISSION_REASON);
            } else {
                if (GalaxySdk.isDebugEnabled())
                    Log.w(TAG, NO_INTERNET_PERMISSION_REASON);
            }
        }
    }

    public static void notNull(Object arg, String name) {
        if (arg == null) {
            throw new NullPointerException("Argument '" + name + "' cannot be null");
        }
    }

    public static void has(Object field, String name, boolean shouldThrow) {
        if (field == null) {
            if (shouldThrow)
                throw new IllegalStateException("No " + name + " found, please set the " + name + ".");
            else {
                if (GalaxySdk.isDebugEnabled()) {
                    Log.w(TAG, "No " + name + " found, please set the " + name + ".");
                }
            }
        }
    }
}
