package com.uphave.galaxy.exception;

/**
 * An Exception indicating that the Galaxy SDK has not been correctly initialized.
 */
public class GalaxySdkNotInitializedException extends GalaxyException {
    /**
     * Constructs a new GalaxySdkNotInitializedException.
     */
    public GalaxySdkNotInitializedException() {
        super();
    }

    /**
     * Constructs a new GalaxyException.
     *
     * @param message the detail message of this exception
     */
    public GalaxySdkNotInitializedException(final String message) {
        super(message);
    }

    /**
     * Constructs a new GalaxySdkNotInitializedException.
     *
     * @param format the format string (see {@link java.util.Formatter#format})
     * @param args   the list of arguments passed to the formatter.
     */
    public GalaxySdkNotInitializedException(final String format, final Object... args) {
        super(format, args);
    }

    /**
     * Constructs a new GalaxySdkNotInitializedException.
     *
     * @param message   the detail message of this exception
     * @param throwable the cause of this exception
     */
    public GalaxySdkNotInitializedException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

    /**
     * Constructs a new GalaxySdkNotInitializedException.
     *
     * @param throwable the cause of this exception
     */
    public GalaxySdkNotInitializedException(final Throwable throwable) {
        super(throwable);
    }
}
