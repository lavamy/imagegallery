package com.uphave.galaxy.exception;

/**
 * Represents an error condition specific to the Galaxy SDK for Android.
 */
public class GalaxyException extends RuntimeException {
    private static final long serialVersionUID = 1;

    /**
     * Constructs a new GalaxyException.
     */
    GalaxyException() {
        super();
    }

    /**
     * Constructs a new GalaxyException.
     *
     * @param format the format string (see {@link java.util.Formatter#format})
     * @param args   the list of arguments passed to the formatter.
     */
    GalaxyException(String format, Object... args) {
        this(String.format(format, args));
    }

    /**
     * Constructs a new GalaxyException.
     *
     * @param message the detail message of this exception
     */
    public GalaxyException(String message) {
        super(message);
    }

    /**
     * Constructs a new GalaxyException.
     *
     * @param message   the detail message of this exception
     * @param throwable the cause of this exception
     */
    public GalaxyException(String message, Throwable throwable) {
        super(message, throwable);
    }

    /**
     * Constructs a new GalaxyException.
     *
     * @param throwable the cause of this exception
     */
    public GalaxyException(Throwable throwable) {
        super(throwable);
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
