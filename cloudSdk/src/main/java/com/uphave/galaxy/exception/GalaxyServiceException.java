package com.uphave.galaxy.exception;

/**
 * Represents an error returned from the Galaxy service in response to a request.
 */
public class GalaxyServiceException extends GalaxyException {
    private static final long serialVersionUID = 1;

    /**
     * Constructs a new GalaxyServiceException.
     *
     * @param message the error message from the request
     */
    public GalaxyServiceException(final String message) {
        super(message);
    }
}
